{
    'name': 'Music',
    'version': '0.0.1',
    'license': 'Other proprietary',
    'category': 'Education',
    "sequence": 4,
    'summary': 'Set the favourite song for contacts',
    'complexity': "easy",
    'author': 'EducativeTech',
    'website': 'https://educativetech.com',
    'depends': [
        'base',
    ],
    'data': [
        'security/ir.model.access.csv',
        'views/song.xml'
    ],
    'demo': [],
    'images': [],
    'installable': True,
    'auto_install': False,
    'application': True,
}
